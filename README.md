# moping

`moping` is a tool that runs [ping](https://www.mongodb.com/docs/manual/reference/command/ping/) commands on MongoDB
to measure network latency between the client and MongoDB server. It is useful when the regular ping command is not
available or suitable for this purpose due to network restrictions.

![Made with VHS](https://vhs.charm.sh/vhs-7KU05kkUOwPmr36hAqLsEg.gif)

It connects and authenticates to a MongoDB server using the MongoDB driver. Once the connection is established,
`moping` sends the no-op [ping](https://www.mongodb.com/docs/manual/reference/command/ping/) command at given
intervals for a defined number of times or until it is interrupted by pressing CTRL-C. It measures the time it takes
for the server to respond to each ping request, which includes the time it takes to process the request and send the
response over the network. At the end, it prints a summary of the ping durations, including minimal, average, and
maximal durations, as well as the standard deviation of all readings.

`moping`'s output and command line arguments resemble those of the regular `ping` command, but they work differently
and require different interpretation of the results. While the regular ping command sends ICMP packets, which can
provide information about each packet's round-trip time and loss, `moping` uses TCP, which cannot provide this level
of detail. Instead, `moping` measures the time it takes for the server to respond to each ping request, which
includes the time it takes to process the request and send the response over the network. This means that `moping`
can measure the network latency between the client and a healthy MongoDB server, but it may not be helpful in diagnosing
network problems or packet loss.

## Install

Pre-built software packages are available for download on
the [releases page](https://gitlab.com/sw0x2A/moping/-/releases).

Docker images are available too. See [Docker](#docker) section for details.

## Usage

```shell
$ moping --help
Run ping commands on MongoDB clusters


Usage: moping [OPTIONS] <URI>

Arguments:
  <URI>  MongoDB connection string as described here: https://www.mongodb.com/docs/manual/reference/connection-string/#connection-string-formats [env: MOPING_MONGODB_URI=]

Options:
      --tls-cafile <TLS_CAFILE>
          The path to the CA file that the MongoDB client should use for TLS. If none is specified, then the driver will use the Mozilla root certificates from the webpki-roots crate [aliases: tlsCAFile]
      --tls-certificate-key-file <TLS_CERTIFICATE_KEY_FILE>
          The path to the certificate file that the MongoDB client should present to the server to verify its identify. If none is specified, then the Client will not attempt to verify its identity to the server [aliases: tlsCertificateKeyFile]
      --username <USERNAME>
          Username for authentication [env: MOPING_MONGODB_USERNAME=]
      --password [<PASSWORD>]
          Password for authentication. Will prompt for password if set without value [env: MOPING_MONGODB_PASSWORD=]
  -q
          Quiet output. Nothing is displayed except the summary lines at startup time and when finished
  -c <COUNT>
          Stop after sending (and receiving) <COUNT> MongoDB ping commands. If this option is not specified, moping will operate until interrupted [default: 0]
      --ignore-first-ping-in-summary
          First ping takes sometimes much longer than the following ones. If this option is specified, moping will not include the first ping in summary
  -i <WAIT>
          Wait <WAIT> milliseconds between sending each ping. The default is to wait for 1000ms (one second) between each packet [default: 1000]
  -h, --help
          Print help
  -V, --version
          Print version
```

### Docker

You can also run it in Docker.
Images are pushed to Gitlab's [Container Registry](https://gitlab.com/sw0x2A/moping/container_registry)
and can be pulled from `registry.gitlab.com/sw0x2a/moping`.

```shell
docker run -t registry.gitlab.com/sw0x2a/moping mongodb://localhost:27017
```

### X.509 authentication

To use X.509 authentication, refer to the example below. Additional TLS settings can be added to the MongoDB connection
string.

```shell
$ moping --tlsCertificateKeyFile X509-cert.pem 'mongodb://localhost:27017/?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority'
Ping from localhost:27017: seq=0 time=4.55ms
Ping from localhost:27017: seq=1 time=2.10ms
Ping from localhost:27017: seq=2 time=2.05ms
^C
--- moping statistics ---
3 MongoDB pings transmitted and received
round-trip min/avg/max/stddev = 2.049ms/2.901ms/4.550ms/1.166ms
```

### SCRAM authentication

---
**NOTE**

Using passwords in command line arguments is not recommended because they may be visible in the process list.
To work around that issue, `moping` will prompt for password if the parameter is set without value or you can provide
the MongoDB connection string and credentials as an environment variable (see examples below).
To avoid credentials being recorded in OS history, prefix the command with a space.
It is recommended to use X.509 certificates instead.

---

You can specify username and password as command-line parameters for SCRAM authentication. Please note that you must
use `--` between `--password` and `uri` as a separator because `--password` can take a value or not and `uri` is the
only positional argument.

```shell
$ moping --username <user> --password -- 'mongodb://example.com:27017/db?retryWrites=true&w=majority'
Password: 
[...]
```

Alternately, you can specify username and password as environment variables...

```shell
$  export MOPING_MONGODB_USERNAME='<user>'
$  export MOPING_MONGODB_PASSWORD='<password>'
$ moping 'mongodb://example.com:27017/db?retryWrites=true&w=majority'
[...]
```

...or set them within the MongoDB connection string.

```shell
$  export MOPING_MONGODB_URI='mongodb://<user>:<password>@example.com:27017/db?retryWrites=true&w=majority'
$ moping
[...]
```
