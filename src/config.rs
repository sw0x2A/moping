use clap::Parser;

#[derive(Parser, PartialEq, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// The path to the CA file that the MongoDB client should use for TLS. If none is specified,
    /// then the driver will use the Mozilla root certificates from the webpki-roots crate.
    #[arg(
        long,
        visible_alias = "tlsCAFile",
        requires("tls_certificate_key_file")
    )]
    pub tls_cafile: Option<String>,

    /// The path to the certificate file that the MongoDB client should present to the server to
    /// verify its identify. If none is specified, then the Client will not attempt to verify its
    /// identity to the server.
    #[arg(long, conflicts_with_all(["username", "password"]), visible_alias = "tlsCertificateKeyFile")]
    pub tls_certificate_key_file: Option<String>,

    /// Username for authentication
    #[arg(long, requires("password"), env = "MOPING_MONGODB_USERNAME")]
    pub username: Option<String>,

    /// Password for authentication.
    /// Will prompt for password if set without value.
    #[arg(long, requires("username"), num_args(0..=1), env = "MOPING_MONGODB_PASSWORD")]
    pub password: Option<String>,

    /// Quiet output.
    /// Nothing is displayed except the summary lines at startup time and when finished.
    #[arg(short)]
    pub quiet: bool,

    /// Stop after sending (and receiving) <COUNT> MongoDB ping commands.
    /// If this option is not specified, moping will operate until interrupted.
    #[arg(short, default_value_t = 0)]
    pub count: usize,

    /// First ping takes sometimes much longer than the following ones.
    /// If this option is specified, moping will not include the first ping in summary.
    #[arg(long)]
    pub ignore_first_ping_in_summary: bool,

    /// MongoDB connection string as described here:
    /// <https://www.mongodb.com/docs/manual/reference/connection-string/#connection-string-formats>
    #[arg(index = 1, env = "MOPING_MONGODB_URI")]
    pub uri: String,

    /// Wait <WAIT> milliseconds between sending each ping.
    /// The default is to wait for 1000ms (one second) between each packet.
    #[arg(short = 'i', default_value_t = 1000)]
    pub wait: u64,
}

pub struct Config {
    pub args: Args,
}

impl Config {
    pub fn new() -> Self {
        let args = Args::parse();
        Config { args }
    }
}

#[test]
fn test_clap_verify_args() {
    use clap::CommandFactory;
    Args::command().debug_assert();
}

#[test]
fn test_clap_defaults() {
    let args = Args::parse_from(["moping", "URI"]);
    assert_eq!(
        args,
        Args {
            tls_cafile: None,
            tls_certificate_key_file: None,
            username: None,
            password: None,
            quiet: false,
            count: 0,
            ignore_first_ping_in_summary: false,
            wait: 1000,
            uri: "URI".to_string(),
        }
    );
}
