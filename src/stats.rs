use std::convert::TryFrom;
use std::time::Duration;

#[derive(Debug, PartialEq)]
pub struct Stats {
    durations: Vec<Duration>,
}

impl Stats {
    /// Create a new [`Stats`] with no data.
    pub fn new() -> Self {
        Self {
            durations: Vec::new(),
        }
    }

    /// Returns the length of durations vector
    // Using u32 because required by `Duration`.
    // using try_from to satisfy `clippy::pedantic`.
    // Not handling error because `u32::MAX` pings should be enough
    // (over one hundred years of secondly pings).
    fn count(&self) -> u32 {
        u32::try_from(self.durations.len()).unwrap()
    }

    /// Calculates mean of durations
    fn mean(&self) -> Duration {
        self.durations.iter().sum::<Duration>() / self.count()
    }

    /// Pushes another element to durations vector
    pub fn push_duration(&mut self, d: Duration) {
        self.durations.push(d);
    }

    /// Calculates population standard deviation of durations
    // I prefer population standard deviation over sample standard deviation because it is
    // also used by `ping` on Linux systems.
    fn std_deviation(&self) -> Duration {
        let count = self.count();
        let mean_duration = self.mean();
        let mean = mean_duration.as_secs_f64();
        let variance = self
            .durations
            .iter()
            .map(|value| {
                let diff = mean - value.as_secs_f64();
                diff * diff
            })
            .sum::<f64>()
            // If you want to return sample standard deviation instead,
            // you should divide by count-1 below.
            // / (f64::from(count) - 1.0);
            / f64::from(count);
        Duration::from_secs_f64(variance.sqrt())
    }

    /// Prints summary
    pub fn print_summary(&self) {
        // Do not print summary if there is no data
        if !self.durations.is_empty() {
            // Unwrap is ok because `durations` cannot be empty
            let min = self.durations.iter().min().unwrap();
            let max = self.durations.iter().max().unwrap();
            let count = self.count();
            let avg = self.mean();
            let stddev = self.std_deviation();

            println!();
            println!("--- moping statistics ---");
            println!("{count} MongoDB pings transmitted and received");
            println!("round-trip min/avg/max/stddev = {min:.3?}/{avg:.3?}/{max:.3?}/{stddev:.3?}");
        }
    }
}

#[cfg(test)]
mod tests {
    use std::f64::consts::SQRT_2;

    use super::*;

    #[test]
    fn test_new() {
        let result = Stats::new();
        assert_eq!(
            result,
            Stats {
                durations: Vec::new()
            }
        );
    }

    #[test]
    fn test_count() {
        let stats = Stats {
            durations: vec![
                Duration::new(1, 0),
                Duration::new(2, 0),
                Duration::new(3, 0),
                Duration::new(4, 0),
                Duration::new(5, 0),
            ],
        };
        let result = stats.count();
        assert_eq!(result, 5);
    }

    #[test]
    fn test_mean() {
        let stats = Stats {
            durations: vec![
                Duration::new(1, 0),
                Duration::new(2, 0),
                Duration::new(3, 0),
                Duration::new(4, 0),
                Duration::new(5, 0),
            ],
        };
        let result = stats.mean();
        assert_eq!(result, Duration::new(3, 0));
    }

    #[test]
    fn test_push_duration() {
        let mut stats = Stats {
            durations: vec![
                Duration::new(1, 0),
                Duration::new(2, 0),
                Duration::new(3, 0),
                Duration::new(4, 0),
            ],
        };
        stats.push_duration(Duration::new(5, 0));
        assert_eq!(
            stats,
            Stats {
                durations: vec![
                    Duration::new(1, 0),
                    Duration::new(2, 0),
                    Duration::new(3, 0),
                    Duration::new(4, 0),
                    Duration::new(5, 0),
                ]
            }
        );
    }

    #[test]
    fn test_std_deviation() {
        let stats = Stats {
            durations: vec![
                Duration::new(1, 0),
                Duration::new(2, 0),
                Duration::new(3, 0),
                Duration::new(4, 0),
                Duration::new(5, 0),
            ],
        };
        let result = stats.std_deviation();
        assert_eq!(result, Duration::from_secs_f64(SQRT_2));
    }
}
