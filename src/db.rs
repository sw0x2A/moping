use std::path::PathBuf;
use std::sync::{Arc, OnceLock};

use mongodb::Client;
use mongodb::bson::{Document, doc};
use mongodb::options::{AuthMechanism, ClientOptions, Credential, Tls, TlsOptions};
use mongodb::{
    event::command::{CommandEventHandler, CommandSucceededEvent},
    options::ServerAddress,
};
use tokio::task_local;

use crate::config::Args;

task_local! {
    static SERVER_ADDRESS: OnceLock<ServerAddress>;
}

struct ServerAddressHandler;

impl CommandEventHandler for ServerAddressHandler {
    fn handle_command_succeeded_event(&self, event: CommandSucceededEvent) {
        let _ = SERVER_ADDRESS.with(|address| address.set(event.connection.address));
    }
}

pub struct DB {
    pub client: Client,
    pub server_address: Option<ServerAddress>,
}

impl DB {
    pub async fn new_from_args(args: &Args) -> mongodb::error::Result<Self> {
        let co = ClientOptionsBuilder::new_from_args(args).await?;
        let client = Client::with_options(co)?;
        Ok(DB {
            client,
            server_address: None,
        })
    }

    pub async fn ping(&mut self) -> mongodb::error::Result<Document> {
        SERVER_ADDRESS
            .scope(OnceLock::new(), async {
                let doc = self
                    .client
                    .database("admin")
                    .run_command(doc! {"ping": 1}, None)
                    .await;
                self.server_address = SERVER_ADDRESS.with(|address| address.get().cloned());
                doc
            })
            .await
    }
}

struct ClientOptionsBuilder;

impl ClientOptionsBuilder {
    async fn new_from_args(args: &Args) -> mongodb::error::Result<ClientOptions> {
        let mut client_options = ClientOptions::parse(&args.uri).await?;
        client_options.command_event_handler = Some(Arc::new(ServerAddressHandler));
        // X.509
        if args.tls_certificate_key_file.is_some() {
            client_options.credential = Some(
                Credential::builder()
                    .mechanism(AuthMechanism::MongoDbX509)
                    .build(),
            );
            let tls_options = match &args.tls_cafile {
                Some(ca_file_path) => TlsOptions::builder()
                    .ca_file_path(PathBuf::from(ca_file_path))
                    .cert_key_file_path(PathBuf::from(
                        args.tls_certificate_key_file.as_ref().unwrap(),
                    ))
                    .build(),
                None => TlsOptions::builder()
                    .cert_key_file_path(PathBuf::from(
                        args.tls_certificate_key_file.as_ref().unwrap(),
                    ))
                    .build(),
            };
            client_options.tls = Some(Tls::Enabled(tls_options));
        }
        // SCRAM
        else if args.username.is_some() {
            let password = args
                .password
                .clone()
                .unwrap_or_else(|| rpassword::prompt_password("Password: ").unwrap());

            client_options.credential = Some(
                Credential::builder()
                    .username(args.username.clone().unwrap())
                    .password(password)
                    .mechanism(AuthMechanism::ScramSha1)
                    .build(),
            );
        }

        Ok(client_options)
    }
}
