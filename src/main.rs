#![warn(clippy::all, clippy::pedantic)]

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use tokio::time::{Duration, Instant, sleep};

use crate::config::Config;
use crate::db::DB;
use crate::stats::Stats;

mod config;
mod db;
mod stats;

#[tokio::main]
async fn main() -> mongodb::error::Result<()> {
    let cfg = Config::new();
    let mut db = DB::new_from_args(&cfg.args).await?;
    let mut stats = Stats::new();

    // Initialise CTRL-C handler
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();
    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");

    // Loop until CTRL-C or i < args.count
    let mut i: usize = 0;
    while running.load(Ordering::SeqCst) {
        let now = Instant::now();
        db.ping().await?;
        let elapsed = now.elapsed();

        if !cfg.args.quiet {
            if let Some(server_address) = &db.server_address {
                println!("Ping from {server_address}: seq={i} time={elapsed:.2?}");
            }
        }
        if !(i == 0 && cfg.args.ignore_first_ping_in_summary) {
            stats.push_duration(elapsed);
        }
        sleep(Duration::from_millis(cfg.args.wait)).await;

        i += 1;
        if cfg.args.count > 0 && i >= cfg.args.count {
            break;
        }
    }
    stats.print_summary();
    Ok(())
}
