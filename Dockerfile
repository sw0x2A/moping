FROM rust:latest as builder
WORKDIR /app
COPY . /app
RUN cargo build --release

FROM gcr.io/distroless/cc-debian12:nonroot
COPY --from=builder /app/target/release/moping /usr/local/bin/
ENTRYPOINT ["moping"]
