# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.6] - 2024-01-15

### Security

- `moping` version 0.1.5 is executed with root privileges in Docker container. Now it runs as nonroot.
  Release 0.1.5 has been removed.

## [0.1.5] - 2024-01-13

### Added

- CI builds and pushes Docker images on releases

### Updated

- Rebuild with latest Rust version and dependencies

## [0.1.4] - 2023-10-14

### Added

- Output shows which host responded to ping

## [0.1.3] - 2023-07-01

### Added

- MongoDB username and password for SCRAM authentication can now be separated from `uri`. Introduced CLI
  parameters `--username` and `--password` (that can also be provided as environment variables
  (`MOPING_MONGODB_USERNAME` and `MOPING_MONGODB_PASSWORD`)). You will get prompted to enter it, if password argument is
  set without value.
- MongoDB connection string (`uri`) can be provided as an environment variable (`MOPING_MONGODB_URI`)

## [0.1.2] - 2023-06-02

### Added

- First release after renaming the project to avoid trademark violation
